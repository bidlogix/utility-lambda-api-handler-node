# Changelog

## 2.0.0

* Requires Node 14+
* Switch @hapi/joi to joi
* Error messages will differ
