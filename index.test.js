const Joi = require('joi')
const { parseEvent, respond } = require('./index')

describe('parseEvent', () => {
  test('valid event with no validation schema', () => {
    const parsedEvent = parseEvent({
      body: '{"foo": "bar"}',
      queryStringParameters: null,
      pathParameters: null
    })

    expect(parsedEvent.errors.length).toEqual(0)
    expect(parsedEvent.event.body.foo).toEqual('bar')
  })

  test('valid event with no validation schema but parameters', () => {
    const parsedEvent = parseEvent({
      body: '{"foo": "bar"}',
      queryStringParameters: {
        bing: 'baz'
      },
      pathParameters: {
        boom: 'bot'
      }
    })

    expect(parsedEvent.errors.length).toEqual(0)
    expect(parsedEvent.event.queryStringParameters.bing).toEqual('baz')
    expect(parsedEvent.event.pathParameters.boom).toEqual('bot')
  })

  test('valid event with no body', () => {
    const parsedEvent = parseEvent({
      body: null,
      queryStringParameters: {},
      pathParameters: {}
    })

    expect(parsedEvent.errors.length).toEqual(0)
    expect(parsedEvent.event.body).toEqual(null)
  })

  test('valid event with validation schema', () => {
    const parsedEvent = parseEvent({
      body: '{"foo": "bar"}',
      queryStringParameters: {},
      pathParameters: {}
    }, Joi.object().keys({
      body: Joi.object().keys({
        foo: Joi.string().required()
      })
    }))

    expect(parsedEvent.errors.length).toEqual(0)
    expect(parsedEvent.event.body.foo).toEqual('bar')
  })

  test('valid event with failing validation schema', () => {
    const parsedEvent = parseEvent({
      body: '{"foo": "bar"}',
      queryStringParameters: {},
      pathParameters: {}
    }, Joi.object({
      body: Joi.object({
        foo: Joi.string().required(),
        bar: Joi.string().required(),
        bing: Joi.string().required()
      })
    }))

    expect(parsedEvent.errors.length).toEqual(2)
  })

  test('invalid event with no validation schema', () => {
    expect.assertions(1)
    try {
      parseEvent({
        body: '{"foo": "ba',
        queryStringParameters: {},
        pathParameters: {}
      }, {})
    } catch (e) {
      expect(e.message).toContain('Invalid JSON')
    }
  })
})

describe('Respond', () => {
  test('All defaults', () => {
    const response = respond()

    expect(response.statusCode).toEqual(200)
  })

  test('Simple success message', () => {
    const response = respond(200, 'Hey')

    expect(response.statusCode).toEqual(200)
    expect(JSON.parse(response.body).message).toEqual('Hey')
  })

  test('Complex success message', () => {
    const response = respond(200, {
      foo: 'bar'
    })

    expect(response.statusCode).toEqual(200)
    expect(JSON.parse(response.body).foo).toEqual('bar')
  })

  test('Message with all inputs', () => {
    const response = respond(500, {
      foo: 'bar'
    }, ['error description'], {
      Authorization: 'Bearer Ralph'
    })

    expect(response.statusCode).toEqual(500)
    expect(JSON.parse(response.body).foo).toEqual('bar')
    expect(JSON.parse(response.body).errors.length).toEqual(1)
    expect(response.headers.Authorization).toEqual('Bearer Ralph')
  })
})
