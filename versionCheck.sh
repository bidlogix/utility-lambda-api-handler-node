#!/bin/bash -e
#
# Checks if version has incremented.
# This script runs only from the root of this repo.
#
############################################################

# compare versions func
function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }

# clone develop of repo
tmp_dir=`pwd`/dist/tmp
printf "Creating $tmp_dir\n"
mkdir -p $tmp_dir
pushd $tmp_dir > /dev/null
git archive --remote=git@bitbucket.org:bidlogix/utility-lambda-api-handler-node.git HEAD package.json | tar -x

# get old and new versions
NEW_PACKAGE_VERSION=$(node -p "require('../../package.json').version")
OLD_PACKAGE_VERSION=$(node -p "require('./package.json').version")

# clear up
popd > /dev/null
rm -rf $tmp_dir

# pass only if version has increased
printf "Old version: ${OLD_PACKAGE_VERSION}\n"
printf "New version: ${NEW_PACKAGE_VERSION}\n"

if version_gt $NEW_PACKAGE_VERSION $OLD_PACKAGE_VERSION; then
  printf "Version number increased.\n"
  exit 0;
else
  printf "Failing. Version number not increased.\n"
  exit 1;
fi
