# utility-lambda-api-handler-node

A generic node package for handling lambda request events in a more predictable and concise fashion

This package is dependent on Joi for the validation

## parseEvent
### API
```
parseEvent(event[, validationSchema])
```

### Example
Input:
```
parseEvent({
  body: `{"foo": "bar"}`,
  queryStringParameters: {
    'bing': 'bong'
  },
  pathParameters: {
    'rasp': 'berry'
  }
}, {
  body: Joi.object().keys({
    foo: Joi.string().required()
  })
})
```

Output:
```
{
  errors: null,
  event: {
    body: ...,
    queryParameters: ...,
    pathParameters: ...
  }
}
```

## respond
Input:
```
respond([statusCode][, message][, errors][, headers])
```

Output
```
{
  statusCode: ...,
  body: message(stringified),
  headers: ...
}
```
