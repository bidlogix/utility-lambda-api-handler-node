module.exports = {
  extends: 'standard',
  env: {
    browser: true,
    jest: true
  }
}
