function _validate (event, validationSchema) {
  if (!validationSchema) return
  const { error } = validationSchema.validate(event, {
    abortEarly: false,
    allowUnknown: true
  })
  return error
}

function _parseBodyAsJSON (event) {
  if (!event.body) {
    return event
  }
  try {
    return { ...event, body: JSON.parse(event.body) }
  } catch (err) {
    throw new Error(`Invalid JSON; ${err.message}`, err)
  }
}

exports.parseEvent = function parseEvent (apiEvent, validationSchema = null) {
  const event = {
    body: apiEvent.body,
    pathParameters: apiEvent.pathParameters || {},
    queryStringParameters: apiEvent.queryStringParameters || {}
  }
  const eventAsJSON = _parseBodyAsJSON(event)
  const validationErrors = _validate(eventAsJSON, validationSchema)?.details?.map(detail => detail.message) || []

  return {
    errors: validationErrors,
    event: eventAsJSON
  }
}

exports.respond = function respond (statusCode = 200, message = null, errors = undefined, headers = {}) {
  // if the message is an object, spread it over the body
  // otherwise just output the message
  const content = (message && typeof message === 'object')
    ? {
        ...message,
        errors
      }
    : {
        message,
        errors
      }

  const response = {
    statusCode,
    body: JSON.stringify(content),
    headers: { 'content-type': 'application/json', ...headers }
  }

  console.info('responding with', response)
  return response
}
